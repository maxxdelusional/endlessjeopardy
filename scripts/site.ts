var txtQuestion:HTMLElement;
var txtAnswer:HTMLElement;
var txtCategory:HTMLElement;

var divLoadingBar:HTMLElement;

let showAnswerTimeoutMS:number = 10000;
let questionIntervalMS:number = 15000;

document.addEventListener("DOMContentLoaded", function() {
    txtQuestion = document.getElementById('txtQuestion')!;
    txtAnswer = document.getElementById('txtAnswer')!;
    txtCategory = document.getElementById('txtCategory')!;
    divLoadingBar = document.getElementById('loading-bar')!;

    loadRandomQuestion();
    setInterval(loadRandomQuestion, questionIntervalMS);
});


function loadRandomQuestion() {
    console.log('loading');

    txtAnswer.innerHTML = '';
    txtAnswer.style.visibility = 'hidden';
    txtQuestion.innerHTML = '';
    txtCategory.innerHTML = '';
    divLoadingBar.style.width = '0%';
divLoadingBar.style.transition = '';

    fetch('http://jservice.io/api/random')
        .then(response => response.json())
        .then(json => {
            console.log(json);
            var data = json[0];
            if (data) {
                txtQuestion.innerHTML = data.question;
                txtAnswer.innerHTML = data.answer;
                txtCategory.innerHTML = data.category.title;

                divLoadingBar.style.transition = 'width ' + showAnswerTimeoutMS + 'ms linear';
                divLoadingBar.style.width = '100%';

                setTimeout(function() {
                    
                   
                    txtAnswer.style.visibility = 'visible';
                }, showAnswerTimeoutMS);

            } 
    });
}