"use strict";
var txtQuestion;
var txtAnswer;
var txtCategory;
var divLoadingBar;
var showAnswerTimeoutMS = 10000;
var questionIntervalMS = 15000;
document.addEventListener("DOMContentLoaded", function () {
    txtQuestion = document.getElementById('txtQuestion');
    txtAnswer = document.getElementById('txtAnswer');
    txtCategory = document.getElementById('txtCategory');
    divLoadingBar = document.getElementById('loading-bar');
    loadRandomQuestion();
    setInterval(loadRandomQuestion, questionIntervalMS);
});
function loadRandomQuestion() {
    console.log('loading');
    txtAnswer.innerHTML = '';
    txtAnswer.style.visibility = 'hidden';
    txtQuestion.innerHTML = '';
    txtCategory.innerHTML = '';
    divLoadingBar.style.width = '0%';
    divLoadingBar.style.transition = '';
    fetch('http://jservice.io/api/random')
        .then(function (response) { return response.json(); })
        .then(function (json) {
        console.log(json);
        var data = json[0];
        if (data) {
            txtQuestion.innerHTML = data.question;
            txtAnswer.innerHTML = data.answer;
            txtCategory.innerHTML = data.category.title;
            divLoadingBar.style.transition = 'width ' + showAnswerTimeoutMS + 'ms linear';
            divLoadingBar.style.width = '100%';
            setTimeout(function () {
                txtAnswer.style.visibility = 'visible';
            }, showAnswerTimeoutMS);
        }
    });
}
